; predicates for special forms

(define special-form?
  (lambda (e type)
    (and (list? e)
         (eq? (car e) type))))

(define lambda-special-form?
  (lambda (e)
    (special-form? e %lambda)))

(define let-special-form?
  (lambda (e)
    (or (special-form? e %let)
        (special-form? e %let*))))

(define set!-special-form?
  (lambda (e)
    (special-form? e %set!)))

(define return-special-form?
  (lambda (e)
    (special-form? e %return)))

(define define-special-form?
  (lambda (e)
    (special-form? e %define)))

; binary operators that but must be written infix
(define scl-infix-binary-operators
  '(+ - * / % ** == != > < >= <=))

(define infix-special-form?
  (lambda (e)
    (and (list? e)
         (member (car e) scl-infix-binary-operators))))

; binary operators that ScLang supports nary forms for
(define scl-non-infix-binary-operators
  '(min max))

(define non-infix-special-form?
  (lambda (e)
    (and (list? e)
         (member (car e) scl-non-infix-binary-operators))))

(define class-special-form?
  (lambda (e)
    (special-form? e %class)))

(define extend-special-form?
  (lambda (e)
    (special-form? e %extend)))

(define if-special-form?
  (lambda (e)
    (special-form? e %if)))
