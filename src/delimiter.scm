; make a list of n minus one commas and one semi-colon
(define make-delimiter-list
  (lambda (n)
    (if (= n 1)
        (list (list %semi-colon %space %newline))
        (append (srfi:make-list (- n 1) (list %comma %space))
                (list (list %semi-colon %space %newline))))))
