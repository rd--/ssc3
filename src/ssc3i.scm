(define-record-type ssc3i-atom
  (fields value))

(define ssc3i?
  (lambda (i)
    (or (ssc3i-atom? i)
        (and (list? i)
             (srfi:every ssc3i? i)))))

(define write-ssc3i-atom
  (lambda (a port)
    (let ((value (ssc3i-atom-value a)))
      (cond ((eq? value %newline)
             (newline port))
            ((eq? value %nothing)
             0)
            ((symbol? value)
             (display value port)) ; symbols cannot use write due of characters that require escapes
            (else
             (write value port))))))

(define write-ssc3i
  (lambda (i port)
    (cond ((ssc3i-atom? i)
           (write-ssc3i-atom i port))
          ((list? i)
           (map (lambda (e) (write-ssc3i e port)) i))
          (else
           (error 'write-ssc3i "illegal input" i port)))))

(define ssc3i->string
  (lambda (i)
    (if (not (ssc3i? i))
        (error 'ssc3i->string "illegal input" i)
        #f)
    (let ((s (srfi:open-output-string)))
      (for-each (lambda (e)
                  (write-ssc3i e s))
                i)
      (srfi:get-output-string s))))
