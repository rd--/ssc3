(define current-ssc3-client
  (srfi:make-parameter #f))

(define make-ssc3-client
  (lambda (host port)
    (udpOpen host port)))

(define ssc3-send
  (lambda (expression)
    (sendMessage (current-ssc3-client) (list "/eval" (ssc3->sc expression)))))

(define scl-eval
  (lambda (text)
    (sendMessage (current-ssc3-client) (list "/eval" text))
    (waitMessage (current-ssc3-client) "/result")))

(define ssc3-eval
  (lambda (expression)
    (scl-eval (ssc3->sc expression))))
