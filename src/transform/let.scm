; (let ((name value) ...) expr...)
(define transform-let
  (lambda (e)
    (let ((variable-declarations
           (lambda (names)
             (let ((n (length names)))
               (build-ssc3i
                'var %space
                (interleave names (make-delimiter-list n))))))
          (variable-assignments
           (lambda (names values)
             (let ((n (length names)))
               (build-ssc3i
                (interleave
                 names
                 (srfi:make-list n (list %space %equal %space))
                 (map transform-entity values)
                 (srfi:make-list n (list %semi-colon %space %newline)))))))
          (names (map car (cadr e)))
          (values (map cadr (cadr e))))
      (build-ssc3i
       %open-brace %newline
       (variable-declarations names)
       (variable-assignments names values)
       (map transform-expression (cddr e))
       %close-brace %dot (transform-entity 'value)))))
