; (return value)
(define transform-return
  (lambda (e)
    (build-ssc3i %hat (transform-entity (cadr e)))))
