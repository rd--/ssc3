; (arg|var|classvar name|(name [default [private]])...)
(define transform-value-declaration
  (lambda (in-class? keyword l)
    (let ((transform-part
           (lambda (e)
             (let* ((is-private?
                     (lambda ()
                       (or (not in-class?)
                           (eq? keyword 'arg)
                           (and (list? e)
                                (= (length e) 3)
                                (eq? (srfi:third e) 'private)))))
                    (get-name
                     (lambda ()
                       (if (is-private?)
                           (if (list? e) (car e) e)
                           (symbol-append '<> (if (list? e) (car e) e))))))
               (if (list? e)
                   (list (get-name)
                         %space %equal %space
                         (transform-entity (cadr e)))
                   (get-name))))))
      (if (null? l)
          (build-ssc3i %nothing)
          (build-ssc3i
           keyword
           %space
           (interleave
            (map transform-part l)
            (make-delimiter-list (length l))))))))
