(define interleave
  (lambda xs
    (concat (transpose xs))))

; #([value...])
(define transform-array
  (lambda (v)
    (let ((l (vector->list v))
          (n (vector-length v)))
      (if (= n 0)
          (build-ssc3i %open-bracket %close-bracket)
          (build-ssc3i
           %open-bracket
           (interleave
            (map transform-entity l)
            (append (srfi:make-list (- n 1) (list %comma %space))
                    (list %close-bracket))))))))
