(define quoted-symbol?
  (lambda (q)
    (and (list? q)
         (= (length q) 2)
         (eq? (car q) 'quote)
         (symbol? (cadr q)))))

(define transform-entity
  (lambda (e)
    (cond ((ssc3i-atom? e)
           e)
          ((number? e)
           (make-ssc3i-atom e))
          ((string? e)
           (make-ssc3i-atom e))
          ((boolean? e)
           (make-ssc3i-atom (if e 'true 'false)))
          ((symbol? e)
           (make-ssc3i-atom e))
          ((quoted-symbol? e)
           (make-ssc3i-atom
            (symbol-append %quote (symbol-append (cadr e) %quote))))
          ((char? e)
           (make-ssc3i-atom
            (symbol-append %dollar-sign (char->symbol e))))
          ((vector? e)
           (transform-array e))
          ((lambda-special-form? e)
           (transform-lambda e))
          ((infix-special-form? e)
           (transform-infix e))
          ((non-infix-special-form? e)
           (transform-non-infix e))
          ((let-special-form? e)
           (transform-let e))
          ((return-special-form? e)
           (transform-return e))
          ((set!-special-form? e)
           (transform-set! e))
          ((class-special-form? e)
           (transform-class e))
          ((extend-special-form? e)
           (transform-extend e))
          ((define-special-form? e)
           (transform-define e))
          ((if-special-form? e)
           (transform-if e))
          ((list? e)
           (transform-dispatch e))
          (else
           (error "transform-entity"
                  "illegal expression"
                  e)))))
