(define build-ssc3i
  (lambda values
    (map (lambda (value)
           (cond ((ssc3i? value)
                  value)
                 ((list? value)
                  (map build-ssc3i value))
                 (else
                  (transform-entity value))))
         values)))
