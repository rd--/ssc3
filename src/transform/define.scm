(define transform-define
  (lambda (e)
    (let ((name (cadr e)))
      (if (list? name)
          (let ((expr (cddr e)))
            (transform-define
             (list 'define (car name)
                   (append (list 'lambda (cdr name))
                           expr))))
          (let ((value (caddr e)))
            (build-ssc3i name
                         %equal
                         (transform-expression value)))))))
