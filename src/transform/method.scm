; (method ((name [default])...) expr)
(define transform-method
  (lambda (name arguments expressions)
    (build-ssc3i
     name %space %open-brace %space %newline
     (transform-value-declaration #f 'arg arguments)
     (map transform-expression expressions)
     %space %close-brace %space %newline)))
