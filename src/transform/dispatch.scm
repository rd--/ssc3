; (method receiver [argument...])
(define transform-dispatch
  (lambda (e)
    (let* ((method (car e))
           (object (cadr e))
           (arguments (cddr e))
           (dot-part (list (transform-entity object) %dot method)))
      (if (null? arguments)
          (build-ssc3i dot-part)
          (build-ssc3i dot-part
                       %open-parenthesis
                       (interleave
                        (map transform-entity arguments)
                        (append (srfi:make-list (- (length arguments) 1)
                                                (list %comma %space))
                                (list %close-parenthesis))))))))
