; clauses are either variable declarations or method definitions
(define transform-clause
  (lambda (clause)
    (cond ((member (car clause) (list %classvar %var))
           (transform-value-declaration #t (car clause) (cdr clause)))
          ((member (car clause) (list %classmethod %method))
           (let* ((signature (cadr clause))
                  (method-name (if (eq? (car clause) %method)
                                   (car signature)
                                   (symbol-append %star (car signature))))
                  (arguments (cdr signature))
                  (expressions (cddr clause)))
             (transform-method method-name arguments expressions)))
          (else
           (error "transform-clause"
                  "illegal clause"
                  clause)))))
