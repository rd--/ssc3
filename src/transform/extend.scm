; (extend class clause...)
(define transform-extend
  (lambda (e)
    (build-ssc3i
     '+ %space (cadr e) %space %open-brace %space %newline
     (map transform-clause (cddr e))
     %space %close-brace %space)))

