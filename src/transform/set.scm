; (set! a b)
(define transform-set!
  (lambda (e)
    (build-ssc3i (transform-entity (cadr e))
                 %space %equal %space
                 (transform-entity (caddr e)))))
