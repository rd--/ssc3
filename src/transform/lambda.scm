; (lambda ((name [default]) ...) expr...)
(define transform-lambda
  (lambda (e)
    (let ((arguments (cadr e))
          (expressions (cddr e)))
      (build-ssc3i %open-brace
                   %newline
                   (transform-value-declaration #f 'arg arguments)
                   (map transform-expression expressions)
                   %close-brace))))
