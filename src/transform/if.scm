(define transform-if
  (lambda (e)
    (let ((a (cadr e))
          (b (caddr e))
          (c (cadddr e)))
      (build-ssc3i %if
                   %open-parenthesis
                   (transform-entity a)
                   %comma
                   %open-brace (transform-entity b) %close-brace
                   %comma
                   %open-brace (transform-entity c) %close-brace
                   %close-parenthesis))))
