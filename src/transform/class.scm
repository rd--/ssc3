; (class (name [parent]) clause...)
(define transform-class
  (lambda (e)
    (let* ((signature (cadr e))
           (class-name (if (list? signature) (car signature) signature))
           (class-parent (if (and (list? signature) (= 2 (length signature)))
                             (cadr signature)
                             %nothing))
           (clauses (cddr e)))
      (build-ssc3i
       (list class-name %space class-parent %space %open-brace %space %newline)
       (map transform-clause clauses)
       %space %close-brace %space %newline))))
