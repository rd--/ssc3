(define transform-expression
  (lambda (e)
    (build-ssc3i (transform-entity e) %semi-colon %space %newline)))
