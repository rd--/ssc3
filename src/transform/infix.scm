; (op left right [value...])
(define transform-nary
  (lambda (op values)
    (build-ssc3i
     %open-parenthesis
     (interleave
      (map transform-entity (srfi:drop-right values 1))
      (srfi:make-list (- (length values) 1) op))
     (transform-entity (last values))
     %close-parenthesis)))

(define transform-infix
  (lambda (e)
    (transform-nary (car e) (cdr e))))

(define transform-non-infix
  (lambda (e)
    (transform-nary (symbol-append (car e) (quote :)) (cdr e))))
