(define ssc3->ssc3i
  (lambda (e)
    (transform-expression e)))

(define ssc3->sc
  (lambda (e)
    (ssc3i->string (ssc3->ssc3i e))))

(define ssc3e->ssc3i
  (lambda (e)
    (transform-entity e)))

(define ssc3e->sc
  (lambda (e)
    (ssc3i->string (ssc3e->ssc3i e))))

; Rewrite the Ssc3 expressions at `input-file-name' as ScLang expressions
; at `output-file-name'.  Expressions are not semi-colon terminated,
; this is required for class and extend expressions.
(define ssc3-file->sc-file
  (lambda (input output)
    (with-input-from-file input
      (lambda ()
        (with-output-to-file
            output
          (lambda ()
            (let loop ((next (read)))
              (if (not (eof-object? next))
                  (begin (display (ssc3e->sc next))
                         (newline)
                         (loop (read)))
                  #f))))))))
