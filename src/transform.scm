(define symbol-append
  (lambda (a b)
  (string->symbol
   (string-append
    (symbol->string a)
    (symbol->string b)))))
