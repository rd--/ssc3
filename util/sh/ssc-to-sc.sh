#!/bin/sh

# Usage: ssc-to-sc SSC-FILE SC-FILE

# mzscheme -mv -M ssc -e "(ssc3-file->sc-file \"$1\" \"$2\")"
echo "(import (ssc3)) (ssc3-file->sc-file \"$1\" \"$2\")" | ikarus
