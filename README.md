ssc3
----

S-Expression SuperCollider3

[Ssc3](http://rohandrape.net/t/ssc3)
implements an s-expression read syntax for the
[SuperCollider](http://audiosynth.com/)
language.  The interaction environment is for
[Emacs](http://gnu.org/software/emacs).
SuperCollider is by James McCartney.  Ssc3 is for
[R6rs](http://www.r6rs.org/)
Scheme.

The Sc2
[analog bubbles](?t=hsc3-graphs&e=lib/scd/graph/jmcc-analog-bubbles.scd)
graph can be written:

~~~~
(let ((freq (midicps (kr LFSaw 0.4 0 24 (kr LFSaw '#(8 7.23) 0 3 80)))))
  (ar CombN (ar SinOsc freq 0 0.1) 0.2 0.2 4))
~~~~

which translates to:

~~~~
{
	{
		var freq;
		freq = LFSaw.kr(0.4, 0, 24, LFSaw.kr([8, 7.23], 0, 3, 80)).midicps;
		CombN.ar(SinOsc.ar(freq, 0, 0.1), 0.2, 0.2, 4);
	}.value;
}.play
~~~~

<!--
There's a brief introduction at
[ssc3.help](?t=ssc3&e=help/ssc3/ssc3.help.ssc3)
-->

© [rohan drape](http://rohandrape.net/),
  2004-2023,
  [gpl](http://gnu.org/copyleft/)

initial announcement:
[[2004/sc-users](?t=ssc3&e=md/announce.text),
 [bham](https://www.listarc.bham.ac.uk/lists/sc-users-2004/msg07891.html)]

<!-- [gmane](http://article.gmane.org/gmane.comp.audio.supercollider.user/7335) -->
