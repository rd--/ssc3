(import (rhs core) (ssc3 core))

; Core data types.
(== (map ssc3->sc '(1 1/2 1.0)) '("1; \n" "1/2; \n" "1.0; \n"))
(== (map ssc3->sc '(#t #f)) '("true; \n" "false; \n"))
(== (ssc3->sc "string") "\"string\"; \n")
(== (ssc3->sc 'symbol) "symbol; \n")
(== (ssc3->sc ''quoted-symbol) "'quoted-symbol'; \n")
(== (ssc3->sc #\c) "$c; \n")

;; Literal array notation.

(ssc3->sc (vector))                     ; => "[]; "

(ssc3->sc (vector 1))                   ; => "[1]; "

(ssc3->sc (vector 1 2 3))               ; => "[1, 2, 3]; "

(ssc3->sc '#(1 2 3))                    ; => "[1, 2, 3]; "

;; Method dispatch, no arguments, single argument, multiple arguments.

(ssc3->sc '(new Array))                 ; => "Array.new; "

(ssc3->sc '(with Array 1))              ; => "Array.with(1); "

(ssc3->sc '(with Array 4 5 6))          ; => "Array.with(4, 5, 6); "

(ssc3->sc '(with Array 1 (new Set)))    ; => "Array.with(1, Set.new); "

(ssc3->sc '(ar SinOsc 440 0 0.1))       ; => "SinOsc.ar(440,0,0.1); "

;; Infix binary math operator translation.

(ssc3->sc '(+ 4 5))                     ; => "(4+5); "

(ssc3->sc '(+ 4 5 6))                   ; => "(4+5+6); "

(ssc3->sc '(+ (* 4 5) (/ 1 2) (- 8 7))) ; => "((4*5)+(1/2)+(8-7)); "

;; Assignment.  Class/instance variable assignment.

(ssc3->sc '(set! a b))                  ; => "a = b; "

(ssc3->sc '(set! freq 440))             ; => "freq = 440; "

(ssc3->sc '(set! (latency s) 0.6))      ; => "s.latency = 0.6; "

;; Return value from method.

(ssc3->sc '(return 5))                  ; => "^5; "

(ssc3->sc '(return (+ this 1)))         ; => "^(this+1); "

;; Variable declaration and initial binding.

(ssc3->sc
 '(let ((freq 440)
        (gain 0.1))
    (ar SinOsc freq 0 gain)))

;; => "var freq, gain; freq = 440; gain = 0.1; SinOsc.ar(freq, 0, gain); "

;; Closure construction.

(ssc3->sc '(lambda () 1))               ; => "{ 1; }; "

(ssc3->sc '(lambda (n) (+ n 1)))        ; => "{arg n; (n+1); }; "

(ssc3->sc '(lambda (a (b 1))
            (effect side) (+ a b)))

;; => "{arg a, b = 1; side.effect; (a+b); }; "

(ssc3->sc
 '(lambda ((freq 440) (gain 0.1))
    (ar SinOsc freq 0 gain)))

;; => "{arg freq = 440, gain = 0.1; SinOsc.ar(freq, 0, gain); }; "

;; Sounding examples.

(ssc3->sc
 '(play (lambda () (ar SinOsc 440 0 0.1))))

;; => "{ SinOsc.ar(440, 0, 0.1); }.play; "

;; Analog bubbles

(ssc3->sc
 '(play
   (lambda ()
     (let ((freq (midicps (kr LFSaw 0.4 0 24 (kr LFSaw #(8 7.23) 0 3 80)))))
       (ar CombN (ar SinOsc freq 0 0.1) 0.2 0.2 4)))))

;; => "{ var freq; freq = LFSaw.kr(0.4, 0, 24, LFSaw.kr([8, 7.23], 0, 3, 80)).midicps; CombN.ar(SinOsc.ar(freq, 0, 0.1), 0.2, 0.2, 4); }.play; "

;; Class definition.

(ssc3->sc
 '(class
   (MyNumber SimpleNumber)
   (classvar (offset 0))
   (method (plusOffset)
           (return (+ this offset)))))

;; => "MyNumber SimpleNumber { classvar <>offset = 0; plusOffset {  ^(this+offset);  }  } ; "

(ssc3->sc
 '(class DotViewer
         (classvar (directory "/tmp") (viewer "open"))
         (classmethod (settings)
                 (postln #(directory viewer)))))

;; => "DotViewer   { classvar <>directory = \"/tmp\", <>viewer = \"open\"; *settings {  [directory, viewer].postln;  }  } ; "

;; Class extension.

(ssc3->sc
 '(extend Number
      (method (postlnTwice)
        (postln this)
        (postln this))))

;; => "+ Number { postlnTwice { this.postln; this.postln;  }  } ; "

(ssc3->sc
 '(extend Number
      (method (postlnN n)
        (iter n (lambda () (postln this))))))

;; => "+ Number { postlnN { arg n; n.iter({ this.postln; });  }  } ; "

;; Note 'if' semantics...

true.if(1+2.postln,2+3.postln);
true.if({1+2.postln},{2+3.postln});

;; Setup the client connection.

(current-ssc3-client (make-ssc3-client "127.0.0.1" 57120))

(ssc3-send (quote (boot s)))

(ssc3-send
 (quote
  (play
   (lambda ()
     (let ((freq (midicps (kr LFSaw 0.4 0 24 (kr LFSaw #(8 7.23) 0 3 80)))))
       (ar CombN (ar SinOsc freq 0 0.1) 0.2 0.2 4))))))

(ssc3-send (quote (stop thisProcess)))

;; File transformation.

(ssc3-file->sc-file "../../ssc3/EvalListener.ssc3" "/tmp/ssc3.help.sc")
