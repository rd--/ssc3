(import (rhs core) (ssc3 core))

(==
 (ssc3i->string
  (transform-class
   '(class (MyNumber SimpleNumber)
      (classvar (offset 0))
      (method (plusOffset)
      (return (+ this offset))))))
 "MyNumber SimpleNumber { \nclassvar <>offset = 0; \nplusOffset { \n^(this+offset); \n } \n } \n")
