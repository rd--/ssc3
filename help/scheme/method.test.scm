(import (rhs core) (ssc3 core))

(==
 (ssc3i->string
  (transform-method
   'method
   '(plusOne SimpleNumber)
   '(return (+ this 1))))
 "method { \narg plusOne, SimpleNumber; \nreturn; \n(this+1); \n } \n")
