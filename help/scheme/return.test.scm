(import (rhs core) (ssc3 core))

(== (ssc3i->string (transform-return '(return 1))) "^1")
(== (ssc3i->string (transform-return '(return (+ a b)))) "^(a+b)")
