(import (rhs core) (ssc3 core))

(== (ssc3i->string (transform-set! '(set! a b))) "a = b")
(== (ssc3i->string (transform-set! '(set! (latency s) (reciprocal 2)))) "s.latency = 2.reciprocal")
