(import (rhs core) (ssc3 core))

(== (ssc3i->string (transform-array '#())) "[]")
(== (ssc3i->string (transform-array '#(1))) "[1]")
(== (ssc3i->string (transform-array (vector 1 2 3))) "[1, 2, 3]")
(== (ssc3i->string (transform-array (vector '(new Set) '(new Array)))) "[Set.new, Array.new]")
