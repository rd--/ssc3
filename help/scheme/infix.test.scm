(import (ssc3 core))

(== (ssc3i->string (transform-infix '(+ 4 5))) "(4+5)")
(== (ssc3i->string (transform-infix '(+ 4 5 6))) "(4+5+6)")
(== (ssc3i->string (transform-infix '(+ 4 (* 5 6) 7))) "(4+(5*6)+7)")
(== (ssc3i->string (transform-non-infix '(min 4 5 3))) "(4min:5min:3)")
(== (ssc3i->string (transform-non-infix '(min 4 (max 5 3)))) "(4min:(5max:3))")
