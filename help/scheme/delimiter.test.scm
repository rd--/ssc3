(import (rhs core) (ssc3 core))

(make-delimiter-list 1) ; => ((|;| | | *newline*))
(make-delimiter-list 3) ; => ((|,| | |) (|,| | |) (|;| | | *newline*))
