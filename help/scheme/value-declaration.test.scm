(import (rhs core) (ssc3 core))

(define (trs-val i j k) (ssc3i->string (transform-value-declaration i j k)))

(== (trs-val #f 'var '()) "")
(== (trs-val #f 'var '(a)) "var a; \n")
(== (trs-val #f 'var '(a b)) "var a, b; \n")
(== (trs-val #f 'var '((a 1))) "var a = 1; \n")
(== (trs-val #t 'var '((a  1) (b 2 private))) "var <>a = 1, b = 2; \n")
(== (trs-val #f 'var '((a  1) (b 2) c)) "var a = 1, b = 2, c; \n")
