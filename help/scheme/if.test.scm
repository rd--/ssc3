(import (ssc3 core))

(==
 (ssc3i->string (ssc3->ssc3i '(if (== n 1) (postln "1") (postln "not 1"))))
 "if((n==1),{\"1\".postln},{\"not 1\".postln}); \n")
