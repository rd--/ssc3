(import (rhs core) (ssc3 core))

(==
 (ssc3i->string
  (transform-extend
   '(extend
        MyNumber
      (classvar (offset2 0))
      (method (plusOffset2)
        (return (+ this offset))))))
 "+ MyNumber { \nclassvar <>offset2 = 0; \nplusOffset2 { \n^(this+offset); \n } \n } ")
