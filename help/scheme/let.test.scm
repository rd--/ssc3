(import (rhs core) (ssc3 core))

(==
 (ssc3i->string
  (transform-let
   '(let ((a 1)
          (b 2))
      (+ a b))))
 "{\nvar a, b; \na = 1; \nb = 2; \n(a+b); \n}.value")

(==
 (ssc3i->string
  (transform-let
   '(let ((a 1)
          (b 2))
      (effect side)
      (+ a b))))
 "{\nvar a, b; \na = 1; \nb = 2; \nside.effect; \n(a+b); \n}.value")
