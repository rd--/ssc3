(import (rhs core) (ssc3 core))

(define (trs-cls e) (ssc3i->string (transform-clause e)))

(== (trs-cls '(classvar (offset 0))) "classvar <>offset = 0; \n")
(== (trs-cls'(method (plusOffset) (return (+ this offset)))) "plusOffset { \n^(this+offset); \n } \n")
(== (trs-cls'(classmethod (plusOne) (return (+ this 1)))) "*plusOne { \n^(this+1); \n } \n")
