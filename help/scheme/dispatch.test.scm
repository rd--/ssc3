(import (rhs core) (ssc3 core))

(define (trs-disp e) (ssc3i->string (transform-dispatch e)))

(== (trs-disp '(boot s)) "s.boot")
(== (trs-disp '(with Array 1)) "Array.with(1)")
(== (trs-disp '(with Array 1 2 3)) "Array.with(1, 2, 3)")
(== (trs-disp '(with Array 1 (new Set) 3)) "Array.with(1, Set.new, 3)")
(== (trs-disp '(play (lambda () (ar SinOsc 440 0 0.1)))) "{\nSinOsc.ar(440, 0, 0.1); \n}.play")
