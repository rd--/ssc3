(import (rhs core) (ssc3 core))

(define (trs-lam e) (ssc3i->string (transform-lambda e)))

(== (trs-lam '(lambda () 1)) "{\n1; \n}")
(== (trs-lam '(lambda (a) (+ a 1))) "{\narg a; \n(a+1); \n}")
(== (trs-lam '(lambda (a) (for side-effect) (+ a 1))) "{\narg a; \nside-effect.for; \n(a+1); \n}")
