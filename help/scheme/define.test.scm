(import (rhs core) (ssc3 core))

(==
 (ssc3i->string (transform-define '(define (~plusOne b) (+ b 1))))
 "~plusOne={\narg b; \n(b+1); \n}; \n")

(==
 (ssc3i->string (transform-define '(define (~printThenPlusOne b) (postln b) (+ b 1))))
 "~printThenPlusOne={\narg b; \nb.postln; \n(b+1); \n}; \n")
