(import (rnrs) (mk-r6rs core))

(define ssc3-file-seq
  '("client"
    "constants"
    "delimiter"
    "predicates"
    "ssc3i"
    "ssc3"
    "transform"
    "transform/array"
    "transform/class"
    "transform/clause"
    "transform/define"
    "transform/dispatch"
    "transform/entity"
    "transform/expression"
    "transform/extend"
    "transform/if"
    "transform/infix"
    "transform/lambda"
    "transform/let"
    "transform/method"
    "transform/return"
    "transform/set"
    "transform/ssc3i"
    "transform/value-declaration"))

(define at-src
  (lambda (x)
    (string-append "../src/" x ".scm")))

(mk-r6rs '(ssc3 core)
	 (map at-src ssc3-file-seq)
	 (string-append (list-ref (command-line) 1) "/ssc3/core.sls")
	 '((rnrs) (rhs core) (sosc core)
           (prefix (srfi :1 lists) srfi:)
           (prefix (srfi :39 parameters) srfi:)
           (prefix (srfi :6 basic-string-ports) srfi:))
	 '()
	 '())

(exit)
